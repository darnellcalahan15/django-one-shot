from django.shortcuts import render, get_object_or_404
from .models import TodoList, TodoItem
from django.db.models import Count
from todos.forms import TodoForm, TodoItemForm
from django.shortcuts import redirect


# Create your views here.
def todo_list(request):
    todos = TodoList.objects.all()
    context = {"todo_list": todos, "count": TodoItem.objects.all().count}
    return render(request, "todos/list.html", context)


def todo_list_details(request, id):
    todo_items = get_object_or_404(TodoList, id=id)
    context = {"todo_items": todo_items}
    return render(request, "todos/details.html", context)


def create_view(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def update(request, id):
    update = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=update)
        if form.is_valid:
            form.save()
            return redirect("todo_list_detail", id=update.id)

    else:
        form = TodoForm(instance=update)

        context = {
            "update": update,
            "form": form,
        }

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    item_delete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        item_delete.delete()
        return redirect("todo_list_list")
    context = {"delete": item_delete}

    return render(request, "todos/delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            list = item.list
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create_item.html", context)


def update_item(request, id):
    item_update = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item_update)
        if form.is_valid:
            form.save()
            return redirect("todo_list_detail", id=item_update.id)

    else:
        form = TodoItemForm(instance=item_update)

        context = {
            "update": item_update,
            "form": form,
        }

    return render(request, "todos/item_edit.html", context)
