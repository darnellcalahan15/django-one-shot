from django.urls import path
from .views import (
    todo_list,
    todo_list_details,
    create_view,
    update,
    todo_list_delete,
    todo_item_create,
    update_item,
)

urlpatterns = [
    path("items/<int:id>/edit/", update_item, name="todo_item_update"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    path("<int:id>/edit/", update, name="todo_list_update"),
    path("create/", create_view, name="todo_list_create"),
    path("<int:id>/", todo_list_details, name="todo_list_detail"),
    path("", todo_list, name="todo_list_list"),
]
